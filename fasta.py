import textwrap


# Class for representing a FASTA data
class Fasta:
    def __init__(self, id, description, sequence):
        self.id = id
        self.description = description
        self.sequence = sequence

    # Formats the FASTA object for writing
    def to_output(self, max_line_length):
        header_line = '> ' + self.id + ' ' + self.description
        output = textwrap.fill(header_line, max_line_length) + '\n'
        output += textwrap.fill(self.sequence, max_line_length) + '\n'
        return output

    def __str__(self):
        output = f'ID: {self.id}\n'
        output += f'Description: {self.description}\n'
        output += f'Sequence length: {len(self.sequence)}\n'
        return output
