#!/usr/bin/env python3
import sys
from fasta import Fasta

# Constants
OUTPUT_OPTION = '--output'
WRAP_OPTION = '--wrap'
MAX_LINE_LENGTH = 80
PRINT_MODE = 0
WRITE_MODE = 1


# Parse the FASTA sequences and process them
def parse_fasta(filename, mode=PRINT_MODE, max_line_length=MAX_LINE_LENGTH):
    try:
        fastas_to_write = []
        with open(filename, 'r') as file:
            # Split the file to sequences and remove empty strings
            fasta_data = [f.strip() for f in file.read().split('>') if f]
            for fasta in fasta_data:
                # Get the first indexes
                first_space_index = fasta.find(' ')
                first_nl_index = fasta.find('\n')
                # Get the id, description and sequence
                id = fasta[:first_space_index]
                description = fasta[first_space_index+1:first_nl_index]
                sequence = fasta[first_nl_index+1:].replace('\n', '')
                # Create a FASTA object
                f = Fasta(id, description, sequence)
                if mode == PRINT_MODE:
                    print(f)
                else:
                    fastas_to_write.append(f.to_output(max_line_length))
        return fastas_to_write
    except Exception as err:
        print('Something happened during reading file', filename)
        print('Error: ', err)
        return []


# Write the formatted FASTA sequences into an output file
def write_fasta(input_file, output_file, max_line_length):
    try:
        with open(output_file, 'w') as file:
            file.writelines(parse_fasta(
                input_file, mode=WRITE_MODE, max_line_length=max_line_length))
    except Exception as err:
        print('Something happened during writing to', output_file, 'file!')
        print('Error: ', err)


if __name__ == '__main__':
    # Get the arguments
    args = sys.argv[1:]

    if len(args) == 0:
        print('Not enough parameters!')
    elif OUTPUT_OPTION in args and args[1] == OUTPUT_OPTION:
        # Check if there's enough arguments
        if len(args) < 3:
            print('Not enough parameters for FASTA writer!')

        # Get the maximum line length if specified
        max_line_length = MAX_LINE_LENGTH
        if WRAP_OPTION in args and args.index(WRAP_OPTION) == 3 and \
                args.index(WRAP_OPTION)+1 < len(args):
            max_line_length = int(args[args.index(WRAP_OPTION)+1])

        # Write the output
        write_fasta(args[0], args[2], max_line_length)
    else:
        # Check if multiple FASTA files were passed
        is_multiple_files = len(args) > 1

        # For each FASTA file parse the file
        for fasta_file in args:
            if is_multiple_files:
                print(f'Parsing {fasta_file}')
            # Parse the file
            parse_fasta(fasta_file)
